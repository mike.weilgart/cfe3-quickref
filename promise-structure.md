# CFEngine 3.12 Quick Reference

## Promise structure

```
  promisetype:
    context::
      "promiser"
        attribute_name_1 => "value 1",
        attribute_name_2 => "$(variable_reference)",
        attribute_name_3 => {
          "element 1",
          "element 2",
        },
        attribute_name_4 => {
          @(slist_variable_reference),
          "final element",
        },
        attribute_name_5 => body_name,
        attribute_name_6 => parameterized_body("arg1", "arg2"),
        attribute_name_7 => function_call("arg1", "arg2");
```

Parts defined by the language (not user choice):

- Promise types
- Attribute names for each promise type

Parts that are user choice:

- Context line (can be any single class or any class expression)
- Promiser
- Right hand side of promise attributes
  - However, the TYPE of the attribute is defined by the language
  - e.g. string, slist, body
  - In above syntax illustration, attributes 1 and 2 show string type
  - Attributes 3 and 4 show slist type
  - Attributes 5 and 6 show bodies
