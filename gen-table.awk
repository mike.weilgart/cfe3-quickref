#!/usr/bin/awk -f
# Author: Mike Weilgart
# Date: 2 August 2019
#
# Purpose: This is intended to parse the raw markdown files found at
# https://github.com/cfengine/documentation/tree/master/reference/promise-types
# and summarize them into markdown tables suitable for rendering on gitlab.com.
#
# Also see the GitLab tables markdown reference:
# https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/user/markdown.md#tables

# Simulate ENDFILE and BEGINFILE blocks;
# code taken from https://www.gnu.org/software/gawk/manual/html_node/Filetrans-Function.html
FILENAME != _oldfilename {
  if (_oldfilename != "")
    endfile(_oldfilename)
  _oldfilename = FILENAME
  beginfile(FILENAME)
}
END { endfile(FILENAME) }

function beginfile(file) {
  # Example final URL:
  # https://docs.cfengine.com/docs/3.12/reference-promise-types-commands.html#useshell
  whichfile = file
  sub(/^.*\//, "", whichfile)
  sub(/\.markdown$/, "", whichfile)
  URLprefix = "https://docs.cfengine.com/docs/3.12/reference-promise-types-" whichfile ".html"
  printf "Attributes for %s promises\n\n", whichfile

  attrname = "Attribute"
  type = "Type"
  desc = "Description"
  printattribute()
  attrname=type=desc="-----"
}

function endfile(file) {
  attributes_reached = 0
  printattribute()
  printf "\n"
}

function printattribute() {
  if (!skip_next_attribute) {
    printf "|%s|%s|%s|\n", attrname, type, desc
  }
  skip_next_attribute = 0
}

/^## Attributes ##/ {
  attributes_reached++
}

# Skip the early parts of all files, until the attributes are reached
!attributes_reached {
  next
}

/^###/ && (NF == 2) {
  # Every time we get to a new header (attribute name), we print the last one.
  # The BEGIN block sets up for encountering the first attribute name
  # and the END block prints the line for the final attribute name.
  printattribute()
  if ($1 == "####") {
    skip_next_attribute++
  }
  attrname = $2
  # Make it a link
  attrname = "[`" attrname "`](" URLprefix "#" attrname ")"
  type = ""
  desc = ""
}

/^\*\*Description:\*\*/,/^ *$/ {
  thisline = $0
  sub(/^\*\*Description:\*\*/, "", thisline)
  desc = desc " " thisline
}

/^\*\*Type:\*\*/ {
  type = $0
  sub(/^\*\*Type:\*\*/, "", type)
}
