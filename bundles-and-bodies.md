# CFEngine 3.12 Quick Reference

## Bundles

Structure for all bundle types is the same.

```
bundle BUNDLETYPE BUNDLENAME {

  promisetype:
    context1::
      "promiser1"
        attribute1 => "value",
        attribute2 => "value";

  # More promises can go here

}
```

A bundle can be defined to accept parameters:

```
bundle BUNDLETYPE BUNDLENAME(paramname1, paramname2) {
  # promises go here
}
```

Parameter names are user choice.

There are only four main BUNDLETYPEs.
They are "agent", "server", "monitor", "common".
In practice most bundles you work with are agent bundles or common bundles.

(The only other BUNDLETYPEs are "edit_line" bundles and "edit_xml" bundles,
which are used by files promises in agent bundles.)

Allowed promise types depend on the bundle type.

## Bodies

Whereas a bundle is a collection of promises, a body is a collection of attributes.

There are many body types but all are defined by the language, not by user choice.

In a body, a semicolon comes after an attribute as an attribute is a complete statement in a body.  (In a bundle, a *promise* is a complete statement, so a semicolon comes after a promise.)

```
body BODYTYPE BODYNAME {
  attribute1 => "value";
  attribute2 => "value";
  attribute3 => { "element1", "element2" };
}
```

Like bundles, bodies can be parameterized:

```
body BODYTYPE BODYNAME(paramname1) {
  # Attributes go here
}
```

Attributes can be made conditional within a body; in practice this is only occasionally used.

```
body BODYTYPE BODYNAME {
  context1::
    attribute1 => "some value";
  context2::
    attribute1 => "different value";
}
```
