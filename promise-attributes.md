Attributes for access promises

|Attribute|Type|Description|
|-----|-----|-----|
|[`admit_hostnames`](https://docs.cfengine.com/docs/3.12/reference-promise-types-access.html#admit_hostnames)||  A list of hostnames or domains that should have access to the object. |
|[`admit_ips`](https://docs.cfengine.com/docs/3.12/reference-promise-types-access.html#admit_ips)||  A list of IP addresses that should have access to the object. |
|[`admit_keys`](https://docs.cfengine.com/docs/3.12/reference-promise-types-access.html#admit_keys)||  A list of RSA keys of hosts that should have access to the object. |
|[`deny_hostnames`](https://docs.cfengine.com/docs/3.12/reference-promise-types-access.html#deny_hostnames)||  A list of hostnames that should be denied access to the object. |
|[`deny_ips`](https://docs.cfengine.com/docs/3.12/reference-promise-types-access.html#deny_ips)||  A list of IP addresses that should be denied access to the object. |
|[`deny_keys`](https://docs.cfengine.com/docs/3.12/reference-promise-types-access.html#deny_keys)||  A list of RSA keys of hosts that should be denied access to the object. |
|[`admit`](https://docs.cfengine.com/docs/3.12/reference-promise-types-access.html#admit)||  The `admit` slist can contain a mix of entries in the syntax of `admit_ips`, `admit_hostnames` and `admit_keys`, and offers the same functionality. It's a legacy attribute that was split in the aforementioned attributes, and it's **not recommended** to use in new policy. |
|[`deny`](https://docs.cfengine.com/docs/3.12/reference-promise-types-access.html#deny)||  The `deny` slist can contain a mix of entries in the syntax of `deny_ips`, `deny_hostnames` and `deny_keys`, and offers the same functionality. It's a legacy attribute that was split in the aforementioned attributes, and it's **not recommended** to use in new policy. Example: |
|[`maproot`](https://docs.cfengine.com/docs/3.12/reference-promise-types-access.html#maproot)||  The `maproot` slist contains host names or IP addresses to grant full read-privilege on the server. |
|[`ifencrypted`](https://docs.cfengine.com/docs/3.12/reference-promise-types-access.html#ifencrypted)||  The `ifencrypted` menu option determines whether the current file access promise is conditional on the connection from the client being encrypted. |
|[`report_data_select`](https://docs.cfengine.com/docs/3.12/reference-promise-types-access.html#report_data_select)||  The `report_data_select` body restricts which data is included for [query][access#resource_type] resources, and allows filtering of data reported to the CFEngine Enterprise server. |
|[`resource_type`](https://docs.cfengine.com/docs/3.12/reference-promise-types-access.html#resource_type)||  The `resource_type` is the type of object being granted access. |
|[`shortcut`](https://docs.cfengine.com/docs/3.12/reference-promise-types-access.html#shortcut)||  For file promisers, the server will give access to the file under its shortcut name. |

Attributes for classes promises

|Attribute|Type|Description|
|-----|-----|-----|
|[`and`](https://docs.cfengine.com/docs/3.12/reference-promise-types-classes.html#and)| `clist`|  Combine class sources with AND |
|[`dist`](https://docs.cfengine.com/docs/3.12/reference-promise-types-classes.html#dist)| `rlist`|  Generate a probabilistic class distribution |
|[`expression`](https://docs.cfengine.com/docs/3.12/reference-promise-types-classes.html#expression)| `class`|  Evaluate string expression of classes in normal form |
|[`or`](https://docs.cfengine.com/docs/3.12/reference-promise-types-classes.html#or)| `clist`|  Combine class sources with inclusive OR |
|[`persistence`](https://docs.cfengine.com/docs/3.12/reference-promise-types-classes.html#persistence)| `int`|  Make the class persistent to avoid re-evaluation |
|[`not`](https://docs.cfengine.com/docs/3.12/reference-promise-types-classes.html#not)| `class`|  Evaluate the negation of string expression in normal form |
|[`scope`](https://docs.cfengine.com/docs/3.12/reference-promise-types-classes.html#scope)| (menu option)|  Scope of the class set by this promise. |
|[`select_class`](https://docs.cfengine.com/docs/3.12/reference-promise-types-classes.html#select_class)| `clist`|  Select one of the named list of classes to define based on host's fully qualified domain name, the primary IP address and the UID that cf-agent is running under. |
|[`xor`](https://docs.cfengine.com/docs/3.12/reference-promise-types-classes.html#xor)| `clist`|  Combine class sources with XOR |

Attributes for commands promises

|Attribute|Type|Description|
|-----|-----|-----|
|[`args`](https://docs.cfengine.com/docs/3.12/reference-promise-types-commands.html#args)| `string`|  Allows to separate the arguments to the command from the command itself. |
|[`arglist`](https://docs.cfengine.com/docs/3.12/reference-promise-types-commands.html#arglist)| `slist`|  Allows to separate the arguments to the command from the  command itself, using an slist. |
|[`contain`](https://docs.cfengine.com/docs/3.12/reference-promise-types-commands.html#contain)| `body contain`|  Allows running the command in a 'sandbox'. |
|[`module`](https://docs.cfengine.com/docs/3.12/reference-promise-types-commands.html#module)| [`boolean`][boolean]|  Set variables and classes based on command output. |

Attributes for databases promises

|Attribute|Type|Description|
|-----|-----|-----|
|[`database_server`](https://docs.cfengine.com/docs/3.12/reference-promise-types-databases.html#database_server)| `body database_server`||
|[`database_type`](https://docs.cfengine.com/docs/3.12/reference-promise-types-databases.html#database_type)| (menu option)|  The `database_type` menu option is a type of database that is to be manipulated. |
|[`database_operation`](https://docs.cfengine.com/docs/3.12/reference-promise-types-databases.html#database_operation)| (menu option)|  The `database_operation` menu option represents the nature of the promise. |
|[`database_columns`](https://docs.cfengine.com/docs/3.12/reference-promise-types-databases.html#database_columns)| `slist`|  A `database_columns` slist defines column definitions to be promised by SQL databases. |
|[`database_rows`](https://docs.cfengine.com/docs/3.12/reference-promise-types-databases.html#database_rows)| `slist`|  `database_rows` is an ordered list of row values to be promised by SQL databases. |
|[`registry_exclude`](https://docs.cfengine.com/docs/3.12/reference-promise-types-databases.html#registry_exclude)| `slist`|  An `registry_exclude` slist contains regular expressions to ignore in key/value verification. |

Attributes for defaults promises

|Attribute|Type|Description|
|-----|-----|-----|
|[`if_match_regex`](https://docs.cfengine.com/docs/3.12/reference-promise-types-defaults.html#if_match_regex)| `string`|  If this [anchored][anchored] regular expression matches the current value of the variable, replace it with default. |

Attributes for files promises

|Attribute|Type|Description|
|-----|-----|-----|
|[`acl`](https://docs.cfengine.com/docs/3.12/reference-promise-types-files.html#acl)| `body acl`||
|[`changes`](https://docs.cfengine.com/docs/3.12/reference-promise-types-files.html#changes)| `body changes`||
|[`copy_from`](https://docs.cfengine.com/docs/3.12/reference-promise-types-files.html#copy_from)| `body copy_from`||
|[`create`](https://docs.cfengine.com/docs/3.12/reference-promise-types-files.html#create)| [`boolean`][boolean]|  true/false whether to create non-existing file |
|[`delete`](https://docs.cfengine.com/docs/3.12/reference-promise-types-files.html#delete)| `body delete`||
|[`depth_search`](https://docs.cfengine.com/docs/3.12/reference-promise-types-files.html#depth_search)| `body depth_search`||
|[`edit_defaults`](https://docs.cfengine.com/docs/3.12/reference-promise-types-files.html#edit_defaults)| `body edit_defaults`||
|[`edit_line`](https://docs.cfengine.com/docs/3.12/reference-promise-types-files.html#edit_line)| [`edit_line`][edit_line]||
|[`edit_template`](https://docs.cfengine.com/docs/3.12/reference-promise-types-files.html#edit_template)| `string`|  Path to Mustache or native-CFEngine template file to expand |
|[`edit_template_string`](https://docs.cfengine.com/docs/3.12/reference-promise-types-files.html#edit_template_string)| `string`|  Mustache string to expand |
|[`edit_xml`](https://docs.cfengine.com/docs/3.12/reference-promise-types-files.html#edit_xml)| [`edit_xml`][edit_xml]||
|[`file_select`](https://docs.cfengine.com/docs/3.12/reference-promise-types-files.html#file_select)| `body file_select`||
|[`file_type`](https://docs.cfengine.com/docs/3.12/reference-promise-types-files.html#file_type)| `string`|  By default, `regular` files are created, when specifying `create => "true"`. You can create fifos through this mechanism as well, by specifying `fifo` in `file_type`. |
|[`link_from`](https://docs.cfengine.com/docs/3.12/reference-promise-types-files.html#link_from)| `body link_from`||
|[`move_obstructions`](https://docs.cfengine.com/docs/3.12/reference-promise-types-files.html#move_obstructions)| [`boolean`][boolean]|  true/false whether to move obstructions to file-object creation |
|[`pathtype`](https://docs.cfengine.com/docs/3.12/reference-promise-types-files.html#pathtype)| (menu option)|  Menu option for interpreting promiser file object |
|[`perms`](https://docs.cfengine.com/docs/3.12/reference-promise-types-files.html#perms)| `body perms`||
|[`rename`](https://docs.cfengine.com/docs/3.12/reference-promise-types-files.html#rename)| `body rename`||
|[`repository`](https://docs.cfengine.com/docs/3.12/reference-promise-types-files.html#repository)| `string`|  Name of a repository for versioning |
|[`template_data`](https://docs.cfengine.com/docs/3.12/reference-promise-types-files.html#template_data)||  The data container to be passed to the template (Mustache or inline_mustache). It can come from a function call like `mergedata()` or from a data container reference like `@(mycontainer)`. |
|[`template_method`](https://docs.cfengine.com/docs/3.12/reference-promise-types-files.html#template_method)||  The template type. |
|[`touch`](https://docs.cfengine.com/docs/3.12/reference-promise-types-files.html#touch)| [`boolean`][boolean]|  true/false whether to touch time stamps on file |
|[`transformer`](https://docs.cfengine.com/docs/3.12/reference-promise-types-files.html#transformer)| `string`|  Command (with full path) used to transform current file (no shell wrapper used) |

Attributes for guest_environments promises

|Attribute|Type|Description|
|-----|-----|-----|
|[`environment_host`](https://docs.cfengine.com/docs/3.12/reference-promise-types-guest_environments.html#environment_host)| `string`|  `environment_host` is a class indicating which physical node will execute this guest machine |
|[`environment_interface`](https://docs.cfengine.com/docs/3.12/reference-promise-types-guest_environments.html#environment_interface)| `body environment_interface`||
|[`environment_resources`](https://docs.cfengine.com/docs/3.12/reference-promise-types-guest_environments.html#environment_resources)| `body environment_resources`||
|[`environment_state`](https://docs.cfengine.com/docs/3.12/reference-promise-types-guest_environments.html#environment_state)| (menu option)|  The `environment_state` defines the desired dynamic state  of the specified environment. |
|[`environment_type`](https://docs.cfengine.com/docs/3.12/reference-promise-types-guest_environments.html#environment_type)| (menu option)|  `environment_type` defines the virtual environment type. |

Attributes for measurements promises

|Attribute|Type|Description|
|-----|-----|-----|
|[`stream_type`](https://docs.cfengine.com/docs/3.12/reference-promise-types-measurements.html#stream_type)| (menu option)|  The datatype being collected. |
|[`data_type`](https://docs.cfengine.com/docs/3.12/reference-promise-types-measurements.html#data_type)| (menu option)|  The datatype being collected. |
|[`history_type`](https://docs.cfengine.com/docs/3.12/reference-promise-types-measurements.html#history_type)| (menu option)|  Whether the data can be seen as a time-series or just an isolated value |
|[`units`](https://docs.cfengine.com/docs/3.12/reference-promise-types-measurements.html#units)| `string`|  The engineering dimensions of this value or a note about its intent used in plots |
|[`match_value`](https://docs.cfengine.com/docs/3.12/reference-promise-types-measurements.html#match_value)| `body match_value`||

Attributes for meta promises

|Attribute|Type|Description|
|-----|-----|-----|

Attributes for methods promises

|Attribute|Type|Description|
|-----|-----|-----|
|[`inherit`](https://docs.cfengine.com/docs/3.12/reference-promise-types-methods.html#inherit)| [`boolean`][boolean]|  If true this causes the sub-bundle to inherit the private classes of its parent |
|[`usebundle`](https://docs.cfengine.com/docs/3.12/reference-promise-types-methods.html#usebundle)| `bundle agent`||
|[`useresult`](https://docs.cfengine.com/docs/3.12/reference-promise-types-methods.html#useresult)| `string`|  Specify the name of a local variable to contain any result/return value from the child |

Attributes for packages-deprecated promises

|Attribute|Type|Description|
|-----|-----|-----|
|[`package_architectures`](https://docs.cfengine.com/docs/3.12/reference-promise-types-packages-deprecated.html#package_architectures)| `slist`|  Select the architecture for package selection |
|[`package_method`](https://docs.cfengine.com/docs/3.12/reference-promise-types-packages-deprecated.html#package_method)| `body package_method`||
|[`package_policy`](https://docs.cfengine.com/docs/3.12/reference-promise-types-packages-deprecated.html#package_policy)| (menu option)|  Criteria for package installation/upgrade on the current system |
|[`package_select`](https://docs.cfengine.com/docs/3.12/reference-promise-types-packages-deprecated.html#package_select)| (menu option)|  Selects which comparison operator to use with `package_version`. |
|[`package_version`](https://docs.cfengine.com/docs/3.12/reference-promise-types-packages-deprecated.html#package_version)| `string`|  Version reference point for determining promised version |

Attributes for packages promises

|Attribute|Type|Description|
|-----|-----|-----|
|[`architecture`](https://docs.cfengine.com/docs/3.12/reference-promise-types-packages.html#architecture)| `string`|  The architecture we want the promise to consider. |
|[`options`](https://docs.cfengine.com/docs/3.12/reference-promise-types-packages.html#options)| `slist`|  Options to pass to the underlying package module. |
|[`policy`](https://docs.cfengine.com/docs/3.12/reference-promise-types-packages.html#policy)| `string`|  Whether the package should be present or absent on the system. |
|[`version`](https://docs.cfengine.com/docs/3.12/reference-promise-types-packages.html#version)| `string`|  The version we want the promise to consider. |
|[`package_module`](https://docs.cfengine.com/docs/3.12/reference-promise-types-packages.html#package_module)| `body package_module`||
|[`yum`](https://docs.cfengine.com/docs/3.12/reference-promise-types-packages.html#yum)|||
|[`apt_get`](https://docs.cfengine.com/docs/3.12/reference-promise-types-packages.html#apt_get)|||
|[`freebsd_ports`](https://docs.cfengine.com/docs/3.12/reference-promise-types-packages.html#freebsd_ports)|||
|[`nimclient`](https://docs.cfengine.com/docs/3.12/reference-promise-types-packages.html#nimclient)|||
|[`pkg`](https://docs.cfengine.com/docs/3.12/reference-promise-types-packages.html#pkg)|||
|[`pkgsrc`](https://docs.cfengine.com/docs/3.12/reference-promise-types-packages.html#pkgsrc)|||
|[`slackpkg`](https://docs.cfengine.com/docs/3.12/reference-promise-types-packages.html#slackpkg)|||
|[`msiexec`](https://docs.cfengine.com/docs/3.12/reference-promise-types-packages.html#msiexec)|||

Attributes for processes promises

|Attribute|Type|Description|
|-----|-----|-----|
|[`process_count`](https://docs.cfengine.com/docs/3.12/reference-promise-types-processes.html#process_count)| `body process_count`||
|[`process_select`](https://docs.cfengine.com/docs/3.12/reference-promise-types-processes.html#process_select)| `body process_select`||
|[`process_stop`](https://docs.cfengine.com/docs/3.12/reference-promise-types-processes.html#process_stop)| `string`|  A command used to stop a running process |
|[`restart_class`](https://docs.cfengine.com/docs/3.12/reference-promise-types-processes.html#restart_class)| `string`|  A class to be defined globally if the process is not running, so that a `command:` rule can be referred to restart the process |
|[`signals`](https://docs.cfengine.com/docs/3.12/reference-promise-types-processes.html#signals)| (option list)|  A list of menu options representing signals to be sent to a process. |

Attributes for reports promises

|Attribute|Type|Description|
|-----|-----|-----|
|[`friend_pattern`](https://docs.cfengine.com/docs/3.12/reference-promise-types-reports.html#friend_pattern)|||
|[`intermittency`](https://docs.cfengine.com/docs/3.12/reference-promise-types-reports.html#intermittency)|||
|[`printfile`](https://docs.cfengine.com/docs/3.12/reference-promise-types-reports.html#printfile)| `body printfile`|  Outputs the content of a file to standard output |
|[`report_to_file`](https://docs.cfengine.com/docs/3.12/reference-promise-types-reports.html#report_to_file)| `string`|  The path and filename to which output should be appended |
|[`bundle_return_value_index`](https://docs.cfengine.com/docs/3.12/reference-promise-types-reports.html#bundle_return_value_index)| `string`|  The promiser is to be interpreted as a literal value that the caller can accept as a result for this bundle; in other words, a return value with array index defined by this attribute. |
|[`lastseen`](https://docs.cfengine.com/docs/3.12/reference-promise-types-reports.html#lastseen)|||
|[`showstate`](https://docs.cfengine.com/docs/3.12/reference-promise-types-reports.html#showstate)|||

Attributes for roles promises

|Attribute|Type|Description|
|-----|-----|-----|
|[`authorize`](https://docs.cfengine.com/docs/3.12/reference-promise-types-roles.html#authorize)| `slist`|  List of public-key user names that are allowed to activate the promised class during remote agent activation |

Attributes for services promises

|Attribute|Type|Description|
|-----|-----|-----|
|[`service_policy`](https://docs.cfengine.com/docs/3.12/reference-promise-types-services.html#service_policy)| `string`|  Policy for service status. |
|[`service_dependencies`](https://docs.cfengine.com/docs/3.12/reference-promise-types-services.html#service_dependencies)| `slist`|  A list of services on which the named service abstraction depends |
|[`service_method`](https://docs.cfengine.com/docs/3.12/reference-promise-types-services.html#service_method)| `body service_method`||

Attributes for storage promises

|Attribute|Type|Description|
|-----|-----|-----|
|[`mount`](https://docs.cfengine.com/docs/3.12/reference-promise-types-storage.html#mount)| `body mount`||
|[`volume`](https://docs.cfengine.com/docs/3.12/reference-promise-types-storage.html#volume)| `body volume`||

Attributes for users promises

|Attribute|Type|Description|
|-----|-----|-----|
|[`description`](https://docs.cfengine.com/docs/3.12/reference-promise-types-users.html#description)||  The `description` string sets the description associated with a user. |
|[`group_primary`](https://docs.cfengine.com/docs/3.12/reference-promise-types-users.html#group_primary)||  The `group_primary` attribute sets the user's primary group. |
|[`groups_secondary`](https://docs.cfengine.com/docs/3.12/reference-promise-types-users.html#groups_secondary)||  The `groups_secondary` attributes sets the user's secondary group membership(s), in addition to his/her primary group. |
|[`home_bundle`](https://docs.cfengine.com/docs/3.12/reference-promise-types-users.html#home_bundle)||  The `home_bundle` attribute specifies a bundle that is evaluated when the user is created. |
|[`home_bundle_inherit`](https://docs.cfengine.com/docs/3.12/reference-promise-types-users.html#home_bundle_inherit)||  The `home_bundle_inherit` attribute specifies if classes set in the current bundle are inherited by the bundle specified in the `home_bundle` attribute. |
|[`home_dir`](https://docs.cfengine.com/docs/3.12/reference-promise-types-users.html#home_dir)||  The `home_dir` attribute associates a user with the given home directory. |
|[`password`](https://docs.cfengine.com/docs/3.12/reference-promise-types-users.html#password)| `body password`|  The `password` attribute specifies a `password` body that contains information about a user's password. |
|[`policy`](https://docs.cfengine.com/docs/3.12/reference-promise-types-users.html#policy)||  The `policy` attribute specifies what state the user account has on the system. |
|[`shell`](https://docs.cfengine.com/docs/3.12/reference-promise-types-users.html#shell)||  The `shell` attribute specifies the user's login shell. |
|[`uid`](https://docs.cfengine.com/docs/3.12/reference-promise-types-users.html#uid)||  The `uid` attribute specifies the user's UID number. |

Attributes for vars promises

|Attribute|Type|Description|
|-----|-----|-----|
|[`policy`](https://docs.cfengine.com/docs/3.12/reference-promise-types-vars.html#policy)| (menu option)|  The policy for (dis)allowing (re)definition of variables |

